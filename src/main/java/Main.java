import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;

/**
 * Created by Ravana on 26.01.2017.
 */
public class Main {
  private static final String downloadFolder = "hpfanficarchive";
  private static final String convertedFolder = "converted";
  private static final String renamedFolder = "renamed";
  private static final String charsetName = "ISO-8859-1";
  private Map<String, String> readCounts = new HashMap<>();
  private Set<String> deletedStories = new LinkedHashSet<>();

  public static void main(String[] args) throws IOException, InterruptedException {
    new Main().run();
  }

  private void run() throws IOException, InterruptedException {
    downloadFanfics();
//    renameFanfics();
//    convertFanfics();
//    convertFanfic(Paths.get(renamedFolder, "101 Uses of the Fidelius Charm Number 62 Dueling by Perspicacity.html"));
    System.out.println(readCounts);

  }

  private void renameFanfics() throws IOException {
    Files.walk(Paths.get(downloadFolder)).forEach(this::renameFanfic);
    System.out.println(deletedStories);
  }

  private void convertFanfics() throws IOException {
    Files.walk(Paths.get(renamedFolder)).forEach(this::convertFanfic);
  }

  private void renameFanfic(Path path) {
    // https://jsoup.org/cookbook/extracting-data/dom-navigation
    if (!path.toString().contains(".html")) {
      return;
    }
    try {
      List<String> lines = Files.readAllLines(path, Charset.forName(charsetName));
      Document doc = Jsoup.parse(String.join("\n", lines));
      String title = doc.select("#pagetitle").text().replaceAll("[^A-Za-z0-9 ._-]", "");
      Path renamedPath = Paths.get(renamedFolder, title + ".html");
      if (title.isBlank() && Files.size(path) < 4000) {
        deletedStories.add(path.getName(path.getNameCount() - 1).toString().replace(".html", ""));
        Files.delete(path);
        System.out.println("Deleted empty file " + path.getFileName());
      } else {
        Files.copy(path, renamedPath, StandardCopyOption.REPLACE_EXISTING);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void convertFanfic(Path path) {
    // https://jsoup.org/cookbook/extracting-data/dom-navigation
    if (!path.toString().contains(".html")) {
      return;
    }
    try {
      List<String> lines = Files.readAllLines(path, Charset.forName(charsetName));
      Document doc = Jsoup.parse(String.join("\n", lines));
      doc.outputSettings().charset(charsetName);
      String title = doc.select("#pagetitle").text().replaceAll("[^A-Za-z0-9 ._-]", "");
      Path convertedPath = Paths.get(convertedFolder, title + ".html");

      Elements content = doc.getElementsByTag("script");
      for (Element element : content) {
        element.remove();
      }
      Node readCountNode = doc.select(".content").get(0).children().stream()
          .filter(e -> e.text().equals("Read:")).findFirst()
          .orElseThrow(() -> new IllegalArgumentException("No read count found"))
          .nextSibling();
      String readCount = ((TextNode) readCountNode).text().strip();
      readCounts.put(title, readCount);
      ((TextNode) readCountNode).text("(Read count stored in separate file)");
      Files.writeString(convertedPath, doc.html(), Charset.forName(charsetName), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void downloadFanfics() throws IOException, InterruptedException {
    int maxId = 2331;
    for (int i = 0; i < maxId; i++) {
      if (Constants.deletedStories.contains(i)) {
        continue;
      }
      System.out.println("Downloading index " + i);
      performRequest("http://www.hpfanficarchive.com/stories/viewstory.php?action=printable&textsize=0&sid=%d&chapter=all", i);
      Thread.sleep(2000);
    }
  }

  private void performRequest(String baseUrl, int storyId) throws IOException {
    String url = String.format(baseUrl, storyId);

    HttpGet get = new HttpGet(url);
    get.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
    get.addHeader("Referer", String.format("http://www.hpfanficarchive.com/stories/viewstory.php?sid=%d", storyId));
    get.addHeader("Host", "www.hpfanficarchive.com");
    get.addHeader("Connection", "keep-alive");
    get.addHeader("Upgrade-Insecure-Requests", "1");
    get.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
    get.addHeader("DNT", "1");
    get.addHeader("Accept-Encoding", "gzip, deflate, sdch");
    get.addHeader("Accept-Language", "en-US,en;q=0.8,et;q=0.6");

    try (CloseableHttpClient httpclient = HttpClients.createDefault(); CloseableHttpResponse response = httpclient.execute(get)) {
      HttpEntity entity = response.getEntity();
      if (entity != null) {
//                System.out.println(entity.getContentLength());
//                System.out.println(EntityUtils.toString(entity));
        Files.copy(entity.getContent(), Paths.get(downloadFolder, Integer.toString(storyId) + ".html"), StandardCopyOption.REPLACE_EXISTING);
      }
    }

  }
}
